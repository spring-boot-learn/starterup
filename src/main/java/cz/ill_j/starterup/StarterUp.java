package cz.ill_j.starterup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class StarterUp {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(StarterUp.class, args);
    }

    @RequestMapping("/hello")
    String sayHello() {
        return "Hello World";
    }
}
